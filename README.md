# [api-platform](https://packagist.org/packages/api-platform/)/[api-pack](https://packagist.org/packages/api-platform/api-pack) (Aliases: api, api-platform)

[**api-platform/api-pack**](https://packagist.org/packages/api-platform/api-pack)
A Pack for API Platform https://api-platform.com

## Admin
* A fancy and fully-featured administration interface builder written in ReactJS supporting APIs documented using Hydra (including all APIs built with API Platform) https://api-platform.com/docs/admin/

## Official documentation
* [*Getting Started with API Platform: Hypermedia and GraphQL API, Admin and Progressive Web App*
  ](https://api-platform.com/docs/distribution)

## Unofficial documentation
### Comparison
* [*Comparing API Platform and Symfony Encore with ReactJS*
  ](https://medium.com/@julbrs/comparing-api-platform-and-symfony-encore-with-reactjs-e4e91e0fdb6c)
  2019-10 Julien Bras

### Tutorials
* [*API Platform up and running in 5 minutes 🚀*
  ](https://dev.to/gremo/api-platform-up-and-running-in-5-minutes-1ko2)
  2023-07 Marco Polichetti (DEV)
* [*Formats Supported By the API Platform*
  ](https://medium.com/swlh/formats-supported-by-the-api-platform-35013626c79f)
  2020-09 Yusuf Biberoğlu
* [*Authentication with VueJs using Symfony and Api Platform – Part 1*
  ](https://stefanoalletti.wordpress.com/2019/08/02/authentication-with-vuejs-using-symfony-and-api-platform-part-1/)
  2019-08 Stefano Alletti
* [*Just another PoC of Symfony on steroids : Api Platform, Vue.js, Mercure and Panther*
  ](https://stefanoalletti.wordpress.com/2019/06/17/just-another-poc-of-symfony-on-steroids%E2%80%A8-api-platform-vue-js-mercure-and-panther/)
  2019-06 Stefano Alletti
* [*Expose a REST API to different kinds of users with Api-Platform*
  ](https://medium.com/@bpolaszek/expose-a-rest-api-to-different-kinds-of-users-with-api-platform-part-1-4-713f3c6db86f)
  2019-06
  [Benoit POLASZEK](https://medium.com/@bpolaszek)
* [*Creating a modern web application using Symfony API Platform*
  ](https://weknowinc.com/blog/creating-modern-web-application-using-symfony-api-platform)
  2018-11 Jesus Manuel Olivas
* [*How to build a Symfony 4 API Platform application from scratch*
  ](https://www.nielsvandermolen.com/symfony-4-api-platform-application/)
  2018-06 nielsvandermolen

### Critics
* [*Why you don’t need JWT*](https://jolicode.com/blog/why-you-dont-need-jwt)
  2019-06
  [gregoire-pineau](https://jolicode.com/equipe/gregoire-pineau)

### Deconstruction
* [*Another way for a Symfony API to ingest POST requests - No Bundle*
  ](https://dev.to/gmorel/another-way-for-a-symfony-api-to-ingest-post-requests-no-bundle-5h15)
  2020-07 Guillaume MOREL


## Related projects in this group
* [symfony/panther](https://gitlab.com/php-packages-demo/symfony-panther)
* [facebook/webdriver](https://gitlab.com/php-packages-demo/facebook-webdriver)
